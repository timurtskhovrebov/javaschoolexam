package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        boolean can = false;
        ArrayList<Integer> elementsIndex = new ArrayList<>();
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }else if (x.size() > y.size() || !y.containsAll(x)) {
            return false;
        } else if (x.isEmpty()) {
            return true;
        }
        for (Object i: x) {
            if (x.contains(i)) {
                elementsIndex.add(y.indexOf(i));
            }
        }
        for (int i=0; i<elementsIndex.size(); i++) {
            if ((i + 1) < elementsIndex.size()) {
                can = elementsIndex.get(i) < elementsIndex.get(i + 1);
                if(!can){
                    return can;
                }
            }
        }
        return can;
    }
}
