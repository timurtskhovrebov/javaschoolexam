package com.tsystems.javaschool.tasks.pyramid;

import javafx.collections.transformation.SortedList;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        int size = inputNumbers.size();
        int rows = 0;
        int cols = 0;
        int result = 0;
        for (int i =0; i < size; i++) {
            result += i;
            if (result == size) {
                rows = i;
                cols = i + i - 1;
                break;
            } else if (result > size
                    || inputNumbers.contains(null)
                    || inputNumbers.get(1)== 0) {
                throw new CannotBuildPyramidException();
            }
        }
        int [][] massive = new int[rows][cols];
        Collections.sort(inputNumbers);
        LinkedList<Integer> sortedInput = new LinkedList<Integer>(inputNumbers);
        for (int i = 0; i < rows; i++) {
            int center = (cols - 1)/2;
            if (i % 2 != 0) {
                for (int k = i; k >= -i; k-=2) {
                    massive[i][center - k] = sortedInput.pollFirst();
                }
            } else {
                for (int k = i; k >= -i; k-=2) {
                        if (i == 0) {
                            massive[i][center] = sortedInput.pollFirst();
                        } else {
                            massive[i][center - k] = sortedInput.pollFirst();
                        }
                }
            }
        }
        return massive;
    }
}
