package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null
                || !statement.matches("^(?:[-+(])?(?:\\d+[.]?\\d*[-*+/][(]?)+\\d+[.]?\\d*[)]?([-*+/]\\d+[.]?\\d*)*$")
                ||!new BracketChecker().validate(statement)) return null;
        String polish = GetExpression(statement);
        double result = 0; //Результат
        Stack<Double> temp = new Stack<>(); //Dhtvtyysq стек для решения

        for (int i = 0; i < polish.length(); i++) //Для каждого символа в строке
        {
            //Если символ - цифра, то читаем все число и записываем на вершину стека
            if (Character.isDigit(polish.toCharArray()[i]))
            {
                String a = "";

                while (!isDelimeter(polish.toCharArray()[i]) && !isOperator(polish.toCharArray()[i])) //Пока не разделитель
                {
                    a += polish.toCharArray()[i]; //Добавляем
                    i++;
                    if (i == polish.length()) break;
                }
                temp.push(Double.parseDouble(a)); //Записываем в стек
                i--;
            } else if (isOperator(polish.toCharArray()[i])) //Если символ - оператор
            {
                //Берем два последних значения из стека
                double a = temp.pop();
                double b = temp.pop();
                if (a == 0) return null;

                switch (polish.toCharArray()[i]) //И производим над ними действие, согласно оператору
                {
                    case '+': result = b + a; break;
                    case '-': result = b - a; break;
                    case '*': result = b * a; break;
                    case '/': result = b / a; break;
                }
                temp.push(result); //Результат вычисления записываем обратно в стек
            }
        }
        if (temp.peek() - Math.round(temp.peek()) == 0){
            return String.valueOf(temp.peek().intValue());
        } else {
            return String.valueOf(temp.peek());
        }
         //Забираем результат всех вычислений из стека и возвращаем его
    }

    String GetExpression(String input) {
        String output = ""; //Строка для хранения выражения
        Stack<Character> operStack = new Stack<Character>(); //Стек для хранения операторов

        for (int i = 0; i < input.length(); i++) {

            //Если символ - цифра, то считываем все число
            if (Character.isDigit(input.toCharArray()[i])) //Если цифра
            {
                //Читаем до разделителя или оператора, что бы получить число
                while (!isDelimeter(input.toCharArray()[i]) && !isOperator(input.toCharArray()[i]))
                {
                    output += input.toCharArray()[i]; //Добавляем каждую цифру числа к нашей строке
                    i++; //Переходим к следующему символу

                    if (i == input.length()) break; //Если символ - последний, то выходим из цикла
                }

                output += " "; //Дописываем после числа пробел в строку с выражением
                i--; //Возвращаемся на один символ назад, к символу перед разделителем
            }

            //Если символ - оператор
            if (isOperator(input.toCharArray()[i])) //Если оператор
            {
                if (input.toCharArray()[i] == '(') //Если символ - открывающая скобка
                    operStack.push(input.toCharArray()[i]); //Записываем её в стек
                else if (input.toCharArray()[i] == ')') //Если символ - закрывающая скобка
                {
                    //Выписываем все операторы до открывающей скобки в строку
                    char s = operStack.pop();

                    while (s != '(')
                    {
                        output += Character.toString(s) + ' ';
                        s = operStack.pop();
                    }
                } else {
                    if (operStack.size() > 0) //Если в стеке есть элементы
                        if (getPriority(input.toCharArray()[i]) <= getPriority(operStack.peek())) //И если приоритет нашего оператора меньше или равен приоритету оператора на вершине стека
                            output += operStack.pop().toString() + " "; //То добавляем последний оператор из стека в строку с выражением

                    operStack.push(input.toCharArray()[i]); //Если стек пуст, или же приоритет оператора выше - добавляем операторов на вершину стека

                }
            }
        }

        //Когда прошли по всем символам, выкидываем из стека все оставшиеся там операторы в строку
        while (operStack.size() > 0)
            output += operStack.pop() + " ";
        "ff".matches("^(?:[-+(])?(?:\\d+[*+/-][(|)]?)+\\d+$");


        return output; //Возвращаем выражение в постфиксной записи
    }

    boolean isOperator(char c) {
        return ("+-/*^()".indexOf(c) != -1);
    }

    boolean isDelimeter(char c) {
        return (" =".indexOf(c) != -1);
    }

    byte getPriority (char c) {
        switch (c)
        {
            case '(': return 0;
            case ')': return 1;
            case '+': return 2;
            case '-': return 3;
            case '*': return 4;
            case '/': return 4;
            case '^': return 5;
            default: return 6;
        }
    }

}
